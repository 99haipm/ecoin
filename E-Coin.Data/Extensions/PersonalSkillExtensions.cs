﻿using E_Coin.Data.Models;
using E_Coin.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;

namespace E_Coin.Data.Extensions
{
    public static partial class PersonalSkillExtensions
    {
        private static IQueryable<PersonalSkill> Filter(this IQueryable<PersonalSkill> query, PersonalFilter filter)
        {
            if (filter.Ids != null)
            {
                query = query.Where(s => filter.Ids.Contains(s.Id)) ;
            }
            if (filter.Name != null && filter.Name.Length > 0)
            {
                query = query.Where(s => s.Name.Contains(filter.Name));
            }
            return query;
        }
        private static IQueryable<PersonalSkill> Sort(this IQueryable<PersonalSkill> query, string sort)
        {
            if (sort != null && sort.Length > 0)
            {
                var asc = sort[0] == 'a';
                var fieldName = sort.Split(" ")[1];
                switch (fieldName)
                {
                    case PersonalSkillSort.Name:
                        if (asc)
                        {
                            query = query.OrderBy(s => s.Name);
                        }
                        else
                        {
                            query = query.OrderByDescending(s => s.Name);
                        }
                        break;
                }
            }
            return query;
        }
        private static IQueryable<PersonalSkill> Pagination(this IQueryable<PersonalSkill> query, int page, int limit)
        {
            if (limit > -1 && page >= 0)
            {
                query = query.Skip(page*limit).Take(limit);
            }
            return query;
        }

        private static object SelectField(this IQueryable<PersonalSkill> query, string[] fields)
        {
            var model = query.ToList();
            var listResult = new List<Dictionary<string, string>>();
            foreach (var l in model)
            {
                var obj = new Dictionary<string,string>();
                    foreach (var field in fields)
                    {
                        switch(field)
                        {
                            case PersonalSkillDetail.INFO:
                                obj["id"] = l.Id;
                                obj["name"] = l.Name;
                                obj["Description"] = l.Description;
                                obj["NumberOfCertification"] = l.NumberOfCertification.ToString();
                                break;

                        }
                    listResult.Add(obj);
                    }
                
                
            }
            var response = new Dictionary<string, object>();
            response["result"] = listResult;
            return response;
        }
        public static object GetData(this IQueryable<PersonalSkill> query, PersonalFilter filter, string sort, string[] fields, int page, int limit)
        {
            query = query.Filter(filter);
            query = query.Sort(sort);
            query = query.Pagination(page, limit);
            var result = query.SelectField(fields);
            return result;
        }
    }
}
