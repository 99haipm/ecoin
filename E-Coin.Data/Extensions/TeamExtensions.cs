﻿using E_Coin.Data.Models;
using E_Coin.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Coin.Data.Extensions
{
    public static partial class TeamExtensions
    {
        #region Filter, sort, pagination
        private static IQueryable<Team> Filter(this IQueryable<Team> query, TeamFilter filter)
        {
            if (filter.Ids != null)
            {
                query = query.Where(s => filter.Ids.Contains(s.Id));
            }
            if (filter.Name_Contains != null && filter.Name_Contains.Length > 0)
            {
                query = query.Where(s => s.Name.Contains(filter.Name_Contains));
            }

            return query;
        }

        private static IQueryable<Team> Sort(this IQueryable<Team> query, string sort)
        {
            if(sort != null && sort.Length > 0)
            {
                var asc = sort[0] == 'a';
                var fieldName = sort.Split(" ")[1];
                switch (fieldName)
                {
                    case TeamFieldsSort.Name:
                        if (asc)
                        {
                            query = query.OrderBy(s => s.Name);
                        }
                        else
                        {
                            query = query.OrderByDescending(s => s.Name);
                        }
                        break;
                }
            }
            return query;
        }

        private static IQueryable<Team> Pagination(this IQueryable<Team> query, int page, int limit)
        {
            if (limit > -1 && page >= 0)
            {
                query = query.Skip(page * limit).Take(limit);
            }
            return query;
        }

        private static object SelectField(this IQueryable<Team> query, string[] fields)
        {
            var model = query.ToList();
            var listResult = new List<Dictionary<string, string>>();
            foreach (var l in model)
            {
                var obj = new Dictionary<string, string>();
                foreach (string field in fields)
                {
                    switch (field)
                    {
                        case TeamFieldsDetail.INFO:
                            obj["id"] = l.Id;
                            obj["name"] = l.Name;
                            obj["status"] = l.Status;
                            break;
                    }
                    listResult.Add(obj);
                }
            }

            var response = new Dictionary<string, object>();
            response["result"] = listResult;
            return response;
        }

        #endregion

        public static object GetData(this IQueryable<Team> query, TeamFilter filter, string sort, string[] fields, int page, int limit)
        {
            query = query.Filter(filter);
            query = query.Pagination(page, limit);
            query = query.Sort(sort);
            var result = query.SelectField(fields);
            return result;
        }
    }
}
