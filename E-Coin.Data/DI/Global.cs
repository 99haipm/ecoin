﻿using AutoMapper;
using E_Coin.Data.Domains;
using E_Coin.Data.Models;
using E_Coin.Data.Repositories;
using E_Coin.Data.Uow;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.DI
{
    public static partial class G
    {
		public static IMapper Mapper { get; private set; }
		private static List<Action<IMapperConfigurationExpression>> MapperConfigs
			= new List<Action<IMapperConfigurationExpression>>();
		public static void ConfigureIoC(IServiceCollection services)
		{
			//MapperConfigs.Add(cfg =>
			//{

			//});
			//ConfigureAutomapper();
			////IoC
			services.AddScoped<IUnitOfWork,UnitOfWork>()
				.AddScoped<DbContext, ECoinContext>()
				.AddScoped<ITeamRepository, TeamRepository>()
				.AddScoped<IRolesRepository, RolesRepository>()
				.AddScoped<IUserRepository, UserRepository>()
				.AddScoped<IPersonalSkillRepository,PersonalSkillRepository>()
				.AddScoped<IEmployeeRepository,EmployeeRepository>()
				.AddScoped<IWalletRepository,WalletRepository>()
				.AddScoped<IWalletTypeRepository,WalletTypeRepository>()
				.AddScoped<PersonalSkillDomain>()
				.AddScoped<TeamDomain>()
				.AddScoped<UserDomain>()
				.AddScoped<JWTDomain>();
		}

		private static void ConfigureAutomapper()
		{
			//AutoMapper
			var mapConfig = new MapperConfiguration(cfg =>
			{
				foreach (var c in MapperConfigs)
				{
					c.Invoke(cfg);
				}
			});
			G.Mapper = mapConfig.CreateMapper();

		}

	}
}
