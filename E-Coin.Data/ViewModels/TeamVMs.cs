﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.ViewModels
{
    public class TeamCreateModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("team_point")]
        public int TeamPoint { get; set; }
    }

    public class TeamFilter
    {
        [JsonProperty("name_contains")]
        public string Name_Contains { get; set; }

        [JsonProperty("ids")]
        public string[] Ids { get; set; }
    }

    public class TeamFieldsSort
    {
        public const string Name = "name";
    }

    public class TeamFieldsDetail
    {
        public const string INFO = "info";
        public const string DETAIL = "detail";
    }

    public class TeamUpdateModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
