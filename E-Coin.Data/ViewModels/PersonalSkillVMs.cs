﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.ViewModels
{
    public class PersonalSkillModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("discription")]
        public string Discription { get; set; }
        [JsonProperty("numberOfCertification")]
        public int NumberOfCertification { get; set; }
        [JsonProperty("userId")]
        public string UserId { get; set; }
    }

    public class PersonalFilter
    {
        public string[] Ids { get; set; }
        public string Name { get; set; }
    }


    public class PersonalSkillDetail
    {
        public const string INFO = "info";
        public const string DETAIL = "detail";
    }
    public class PersonalSkillSort
    {
        public const string Name = "name";
    }
    public class PersonalSkillUpdate
    {
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("numberOfCertificate")]
        public int numberOfCertificate { get; set; }
    }
}
