﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.ViewModels
{
    public class GeneralUser
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Role { get; set; }
        public bool Status { get; set; }
        public string Position { get; set; }
    }

    public class UserCreateModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("Fullname")]
        public string Fullname { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("Facebook")]
        public string Facebook { get; set; }
        [JsonProperty("Phone")]
        public string Phone { get; set; }
        [JsonProperty("Address")]
        public string Address { get; set; }
        [JsonProperty("Roles")]
        public string[] Roles { get; set; }
    }

    public class UserModelView
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Status { get; set; }
        public string Email { get; set; }

        public string Phone { get; set; }
        public string Address {get;set;}
        public string Sex { get; set; }
        public string BOD { get; set; }
        public List<WalletViewModel> WalletDetail { get; set; }
    }
}
