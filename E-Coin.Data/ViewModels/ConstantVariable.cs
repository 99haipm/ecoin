﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.ViewModels
{
    public class ConstantVariableStatus
    {
        public const string ACTIVE = "active";
        public const string DISABLE = "disable";

    }

    public class ConstantVariableUserStatus
    {
        public const string WORKING = "working";
        public const string OFF = "off";
    }
}
