﻿using E_Coin.Data.Models;
using E_Coin.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.Repositories
{
    public partial interface ITeamRepository : IBaseRepository<Team, string>
    {
        #region Create
        Team PrepareCreate(TeamCreateModel model);
        Team CreateTeam(TeamCreateModel team);
        #endregion

        #region Edit
        Team Edit(Team entity, TeamUpdateModel model);
        #endregion
    }

    public partial class TeamRepository : BaseRepository<Team, string>, ITeamRepository
    {

        public TeamRepository(DbContext context) : base(context)
        {

        }
        public Team CreateTeam(TeamCreateModel model)
        {
            Team team = PrepareCreate(model);
            return Create(team).Entity;
        }

        public Team Edit(Team entity, TeamUpdateModel model)
        {
            entity.Name = model.Name;
            entity.Description = model.Description;
            return entity;
        }

        public Team PrepareCreate(TeamCreateModel model)
        {
            Team team = new Team
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.Name,
                Description = model.Description,
                Status = ConstantVariableStatus.ACTIVE,
                TeamPoint = model.TeamPoint
            };
            return team;
        }
    }
}
