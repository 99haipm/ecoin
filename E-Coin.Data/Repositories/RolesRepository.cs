﻿using E_Coin.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Coin.Data.Repositories
{
   public partial interface IRolesRepository
   {
        Role FindById(string id);
   }

    
    public partial class RolesRepository : BaseRepository<Role,string>, IRolesRepository
    {
        public RolesRepository(DbContext context) : base(context)
        {

        }

        public Role FindById(string id)
        {
            return Get().FirstOrDefault(r => r.Id == id);
        }
    }


}
