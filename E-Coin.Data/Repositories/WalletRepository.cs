﻿using E_Coin.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Coin.Data.Repositories
{
    public partial interface IWalletRepository
    {
        List<Wallet> GetWalletsByUser(string userId);
    }

    public partial class WalletRepository : BaseRepository<Wallet, string>, IWalletRepository
    {
        public WalletRepository(DbContext context) : base(context)
        {

        }

        public List<Wallet> GetWalletsByUser(string userId)
        {
            return Get().Where(w => w.EmpId == userId).ToList();
        }
    }
}
