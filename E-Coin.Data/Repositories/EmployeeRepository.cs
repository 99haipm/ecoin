﻿using E_Coin.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Coin.Data.Repositories
{
    public partial interface IEmployeeRepository
    {
        Employee GetById(string id);
    }
    public partial class EmployeeRepository : BaseRepository<Employee, string>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext context) : base(context)
        {

        }

        public Employee GetById(string id)
        {
            return Get().FirstOrDefault(e => e.Id == id);
        }
    }
}
