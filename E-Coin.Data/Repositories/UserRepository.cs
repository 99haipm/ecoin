﻿using E_Coin.Data.Models;
using E_Coin.Data.Uow;
using E_Coin.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Coin.Data.Repositories
{
    public partial interface IUserRepository : IBaseRepository<User, string>
    {
        User CreateUser(UserCreateModel model);
        User PrepareCreate(UserCreateModel model);

        User FindById(string username);
    }

    public partial class UserRepository : BaseRepository<User,string> , IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {

        }

        public User CreateUser(UserCreateModel model)
        {
            var user = PrepareCreate(model);
            Create(user);
            SaveChanges();
            return user;
        }

        public User FindById(string username)
        {
            return Get().FirstOrDefault(u => u.Username == username);
        }

        public User PrepareCreate(UserCreateModel model)
        {
            User user = new User()
            {
                Id = Guid.NewGuid().ToString(),
                Username = model.Username,
                Fullname = model.Fullname,
                Email = model.Email,
                Facebook = model.Facebook,
                Phone = model.Phone,
                Address = model.Address,
                Status = ConstantVariableUserStatus.WORKING,
            };
            return user;
        }
    }
}
