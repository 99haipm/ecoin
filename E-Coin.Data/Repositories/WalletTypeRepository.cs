﻿using E_Coin.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.Repositories
{
    public partial interface  IWalletTypeRepository
    {

    }

    public partial class WalletTypeRepository : BaseRepository<WalletType, string>, IWalletTypeRepository
    {
        public WalletTypeRepository(DbContext context) : base(context)
        {

        }

    }
}
