﻿using E_Coin.Data.Models;
using E_Coin.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.Repositories
{
    public partial interface IPersonalSkillRepository : IBaseRepository<PersonalSkill, string>
    {
        #region Create
        PersonalSkill PreparePersonalSkill(PersonalSkillModel model);
        PersonalSkill CreatePersonalSkill(PersonalSkillModel personalSkills);
        #endregion
        #region Update
        PersonalSkill Edit(PersonalSkill perSkill, PersonalSkillUpdate model);
        #endregion
    }

    public partial class PersonalSkillRepository : BaseRepository<PersonalSkill, string>,IPersonalSkillRepository
    {
        public PersonalSkillRepository(DbContext context) : base(context)
        {

        }

        public PersonalSkill CreatePersonalSkill(PersonalSkillModel model)
        {
            PersonalSkill personalSkills = PreparePersonalSkill(model);
            return Create(personalSkills).Entity;
        }

        
        public PersonalSkill PreparePersonalSkill(PersonalSkillModel model)
        {
            PersonalSkill personalSkills = new PersonalSkill
            {
                Id = Guid.NewGuid().ToString(),
                Name = model.Name,
                Description = model.Discription,
                NumberOfCertification = model.NumberOfCertification,
                EmpId = model.UserId
            };
            return personalSkills;
        }

        public PersonalSkill Edit(PersonalSkill perSkill, PersonalSkillUpdate model)
        {
            perSkill.Name = model.name;
            perSkill.Description = model.Description;
            perSkill.NumberOfCertification = model.numberOfCertificate;
            return perSkill;
        }

    }
}
