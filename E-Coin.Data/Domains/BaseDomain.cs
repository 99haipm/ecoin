﻿using E_Coin.Data.Uow;
using System;
using System.Collections.Generic;
using System.Text;

namespace E_Coin.Data.Domains
{
    public abstract class BaseDomain
    {
        protected readonly IUnitOfWork uow;

        public BaseDomain(IUnitOfWork uow)
        {
            this.uow = uow;
        }
    }
}
