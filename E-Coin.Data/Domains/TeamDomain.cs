﻿using E_Coin.Data.Extensions;
using E_Coin.Data.Models;
using E_Coin.Data.Repositories;
using E_Coin.Data.Uow;
using E_Coin.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Coin.Data.Domains
{
    public class TeamDomain : BaseDomain
    {
        public TeamDomain(IUnitOfWork uow) : base(uow)
        {
        }

        public Team Create(TeamCreateModel model)
        {
            return uow.GetService<ITeamRepository>().CreateTeam(model);
        }

        public DbSet<Team> Get()
        {
            return uow.GetService<ITeamRepository>().Get();
        }

        public object GetTeam(TeamFilter filter, string sort, string[] fields, int page, int limit)
        {
            var query = Get();
            return query.GetData(filter, sort, fields, page, limit);
        }

        public Team Update(string id,TeamUpdateModel model)
        {
            var repo = uow.GetService<ITeamRepository>();
            var team = repo.Get().Where(s => s.Id == id).FirstOrDefault();
            if(team != null)
            {
                var updatedTeam = repo.Edit(team, model);
                return repo.Update(updatedTeam).Entity;
            }
            return null;
        }

        public Team Delete(string id)
        {
            var repo = uow.GetService<ITeamRepository>();
            var team = repo.Get().Where(s => s.Id == id).FirstOrDefault();
            team.Status = ConstantVariableStatus.DISABLE;
            return team;
        }
    }
}
