﻿using E_Coin.Data.Models;
using E_Coin.Data.Repositories;
using E_Coin.Data.Uow;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace E_Coin.Data.Domains
{
    public class JWTDomain : BaseDomain
    {
        public JWTDomain(IUnitOfWork uow) : base(uow)
        {

        }
        public object DecodeJwt(string jwt)
        {
            var handler = new JwtSecurityTokenHandler();
            var result = handler.ReadJwtToken(jwt) as JwtSecurityToken;
            var currentClaims = result.Claims.ToList();
            string username = currentClaims.FirstOrDefault(t => t.Type == "email").Value;
            if (UserExisted(username))
            {
                string[] roles = GetRoleOfUser(username);
                foreach(string role in roles)
                {
                    Claim newClaim = new Claim(ClaimTypes.Role, role);
                    currentClaims.Add(newClaim);
                }
                currentClaims.Add(new Claim(ClaimTypes.Name, username));
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(currentClaims);
                var key = Encoding.ASCII.GetBytes("Ecoin_Fucking_Secret");
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = claimsIdentity,
                    Issuer = "Ecoin_Issuer",
                    Audience = "Ecoin_Audience",
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var newToken = handler.CreateToken(tokenDescriptor);
                return handler.WriteToken(newToken);
            }
            return "User not found";
        }

        private bool UserExisted(string username)
        {
            var userRepo = uow.GetService<IUserRepository>();
            return userRepo.FindById(username) != null;
        }

        private string[] GetRoleOfUser(string username)
        {
            var userRepo = uow.GetService<IUserRepository>();
            var roles = userRepo.FindById(username).UserRole.Select(r => r.Role.Name).ToArray();
            return roles;
        }

    }
}
