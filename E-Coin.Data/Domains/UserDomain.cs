﻿using E_Coin.Data.Models;
using E_Coin.Data.Repositories;
using E_Coin.Data.Uow;
using E_Coin.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

namespace E_Coin.Data.Domains
{
    public class UserDomain : BaseDomain
    {
        public UserDomain(IUnitOfWork uow ): base(uow)
        {

        }

        public string RegisterUser(UserCreateModel model)
        {
            var userRepo = uow.GetService<IUserRepository>();
            var newUser = userRepo.CreateUser(model);
            AddRoleToUser(newUser, model.Roles);
            return newUser.Id;
        }

        public string AddRoleToUser(User user,string[] roles)
        {
            var roleRepo = uow.GetService<IRolesRepository>();
            foreach(string roleId in roles)
            {
                var role = roleRepo.FindById(roleId);
                if(role != null)
                {
                    user.UserRole.Add(new UserRole
                    {
                        RoleId = roleId,
                        UserId = user.Id
                    });
                }
            }
            uow.SaveChanges();
            return user.Id;
        }

        public object GetUserDetail(string token)
        {
            var userRepo = uow.GetService<IUserRepository>();
            var empRepo = uow.GetService<IEmployeeRepository>();
            var walletRepo = uow.GetService<IWalletRepository>();
            var handler = new JwtSecurityTokenHandler();
            var result = handler.ReadJwtToken(token) as JwtSecurityToken;
            var currentClaims = result.Claims.ToList();
            string username = currentClaims.FirstOrDefault(t => t.Type == "email").Value;

            var user = userRepo.Get().FirstOrDefault(u => u.Username == username);
            if(user == null)
            {
                return new
                {
                    message = "Not found user"
                };
            }
            var emp = empRepo.GetById(user.Id);
            var wallets = walletRepo.GetWalletsByUser(user.Id);
            List<WalletViewModel> walletViews = new List<WalletViewModel>();
            foreach(var wallet in wallets)
            {
                walletViews.Add(new WalletViewModel {
                    Name = wallet.Type,
                    Value = wallet.Value
                }); 
            }

            var returnResult = new UserModelView
            {
                Username = user.Username,
                Fullname = user.Fullname,
                Address = emp.Address,
                Phone = user.Phone,
                Email = user.Email,
                Status = emp.Status,
                BOD = emp.Bod.ToString(),
                Sex = emp.Sex ? "Male" : "Female",
                WalletDetail = walletViews
            };

            return returnResult;
        }
    }
}
