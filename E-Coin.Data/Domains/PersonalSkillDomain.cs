﻿using E_Coin.Data.Extensions;
using E_Coin.Data.Models;
using E_Coin.Data.Repositories;
using E_Coin.Data.Uow;
using E_Coin.Data.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Coin.Data.Domains
{
    public class PersonalSkillDomain : BaseDomain
    {
        public PersonalSkillDomain(IUnitOfWork uow) : base(uow)
        {

        }

        public PersonalSkill Create(PersonalSkillModel model)
        {
            return uow.GetService<IPersonalSkillRepository>().CreatePersonalSkill(model);
        }
        public DbSet<PersonalSkill> Get()
        {
            return uow.GetService<IPersonalSkillRepository>().Get();
        }
        public object GetPersonalSkill(PersonalFilter filter, string sort, string[] fields, int page, int limit)
        {
            var query = Get();
            return query.GetData(filter, sort, fields, page, limit);
        }
        public PersonalSkill Update(string id, PersonalSkillUpdate model)
        {
            var repo = uow.GetService<IPersonalSkillRepository>();
            var perSkill = repo.Get().Where(s => s.Id == id).FirstOrDefault();
            if (perSkill != null)
            {
                var updatePerSkill = repo.Edit(perSkill, model);
                return repo.Update(updatePerSkill).Entity;
            }
            return null;
        }

        public PersonalSkill Delete(string id)
        {
            var repo = uow.GetService<IPersonalSkillRepository>();
            var perSkill = repo.Get().Where(s => s.Id == id).FirstOrDefault();
            if (perSkill != null)
            {
                return repo.Remove(perSkill).Entity;
            }
            return null;
        }
    }
}
