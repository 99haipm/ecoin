﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class LevelHistory
    {
        public string UserId { get; set; }
        public string SublevelId { get; set; }
        public DateTime Datetime { get; set; }
        public string FromLevel { get; set; }
        public string ToLevel { get; set; }

        public virtual SubLevel Sublevel { get; set; }
        public virtual Employee User { get; set; }
    }
}
