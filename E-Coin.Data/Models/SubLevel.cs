﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class SubLevel
    {
        public SubLevel()
        {
            LevelDetail = new HashSet<LevelDetail>();
            LevelHistory = new HashSet<LevelHistory>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string PositionId { get; set; }
        public double SalaryLevel { get; set; }

        public virtual Level Position { get; set; }
        public virtual ICollection<LevelDetail> LevelDetail { get; set; }
        public virtual ICollection<LevelHistory> LevelHistory { get; set; }
    }
}
