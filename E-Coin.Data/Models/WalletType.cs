﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class WalletType
    {
        public byte[] Id { get; set; }
        public string TypeName { get; set; }
        public string WalletId { get; set; }

        public virtual Wallet Wallet { get; set; }
    }
}
