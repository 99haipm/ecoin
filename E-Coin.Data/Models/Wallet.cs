﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class Wallet
    {
        public Wallet()
        {
            Transaction = new HashSet<Transaction>();
            WalletType = new HashSet<WalletType>();
        }

        public string Id { get; set; }
        public string Type { get; set; }
        public int Value { get; set; }
        public string EmpId { get; set; }

        public virtual Employee Emp { get; set; }
        public virtual ICollection<Transaction> Transaction { get; set; }
        public virtual ICollection<WalletType> WalletType { get; set; }
    }
}
