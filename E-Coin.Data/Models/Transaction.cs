﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class Transaction
    {
        public string Id { get; set; }
        public string WalletId { get; set; }
        public DateTime TimeChange { get; set; }
        public string Description { get; set; }

        public virtual Wallet Wallet { get; set; }
    }
}
