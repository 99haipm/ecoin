﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class EmpPay
    {
        public string EmployeeId { get; set; }
        public string PayPeriodId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual PayPeriod PayPeriod { get; set; }
    }
}
