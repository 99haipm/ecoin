﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class Level
    {
        public Level()
        {
            Employee = new HashSet<Employee>();
            SubLevel = new HashSet<SubLevel>();
        }

        public string Id { get; set; }
        public string Description { get; set; }
        public int HourRate { get; set; }
        public int MinHotPoint { get; set; }
        public int BasicCash { get; set; }
        public int CoinUpTo { get; set; }
        public int UpLevelReward { get; set; }
        public int RetainerBonus { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<SubLevel> SubLevel { get; set; }
    }
}
