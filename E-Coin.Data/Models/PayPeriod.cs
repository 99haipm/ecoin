﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class PayPeriod
    {
        public PayPeriod()
        {
            EmpPay = new HashSet<EmpPay>();
            PayCheck = new HashSet<PayCheck>();
        }

        public string Id { get; set; }
        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Name { get; set; }
        public double Total { get; set; }
        public string CompanyId { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<EmpPay> EmpPay { get; set; }
        public virtual ICollection<PayCheck> PayCheck { get; set; }
    }
}
