﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class TeamDetail
    {
        public string EmpId { get; set; }
        public string TeamId { get; set; }
        public string RoleInTeam { get; set; }

        public virtual Employee Emp { get; set; }
        public virtual Team Team { get; set; }
    }
}
