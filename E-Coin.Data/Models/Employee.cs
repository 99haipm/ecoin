﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class Employee
    {
        public Employee()
        {
            EmpPay = new HashSet<EmpPay>();
            LevelHistory = new HashSet<LevelHistory>();
            PayCheck = new HashSet<PayCheck>();
            PersonalSkill = new HashSet<PersonalSkill>();
            TeamDetail = new HashSet<TeamDetail>();
            Wallet = new HashSet<Wallet>();
        }

        public string Id { get; set; }
        public string Fullname { get; set; }
        public string Address { get; set; }
        public DateTime DateJoin { get; set; }
        public bool Sex { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
        public DateTime Bod { get; set; }
        public string CompanyId { get; set; }
        public string LevelId { get; set; }

        public virtual Company Company { get; set; }
        public virtual User IdNavigation { get; set; }
        public virtual Level Level { get; set; }
        public virtual ICollection<EmpPay> EmpPay { get; set; }
        public virtual ICollection<LevelHistory> LevelHistory { get; set; }
        public virtual ICollection<PayCheck> PayCheck { get; set; }
        public virtual ICollection<PersonalSkill> PersonalSkill { get; set; }
        public virtual ICollection<TeamDetail> TeamDetail { get; set; }
        public virtual ICollection<Wallet> Wallet { get; set; }
    }
}
