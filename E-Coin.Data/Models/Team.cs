﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class Team
    {
        public Team()
        {
            TeamDetail = new HashSet<TeamDetail>();
            TeamPointReport = new HashSet<TeamPointReport>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TeamPoint { get; set; }
        public string Status { get; set; }

        public virtual ICollection<TeamDetail> TeamDetail { get; set; }
        public virtual ICollection<TeamPointReport> TeamPointReport { get; set; }
    }
}
