﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class PayCheck
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string EmpId { get; set; }
        public DateTime DateFinalize { get; set; }
        public int P1 { get; set; }
        public int P2 { get; set; }
        public int P3 { get; set; }
        public double Total { get; set; }
        public string Performance { get; set; }
        public string EvaluateRetrainer { get; set; }
        public int TeamPointShare { get; set; }
        public int? UpLevelReward { get; set; }
        public double WorkingHour { get; set; }
        public string PayPeriodId { get; set; }

        public virtual Employee Emp { get; set; }
        public virtual PayPeriod PayPeriod { get; set; }
    }
}
