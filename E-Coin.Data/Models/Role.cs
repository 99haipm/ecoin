﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class Role
    {
        public Role()
        {
            UserRole = new HashSet<UserRole>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string PermissionDetail { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
