﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class User
    {
        public User()
        {
            UserRole = new HashSet<UserRole>();
        }

        public string Id { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
