﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class Company
    {
        public Company()
        {
            Employee = new HashSet<Employee>();
            PayPeriod = new HashSet<PayPeriod>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<PayPeriod> PayPeriod { get; set; }
    }
}
