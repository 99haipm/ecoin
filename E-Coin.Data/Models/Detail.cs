﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class Detail
    {
        public Detail()
        {
            LevelDetail = new HashSet<LevelDetail>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<LevelDetail> LevelDetail { get; set; }
    }
}
