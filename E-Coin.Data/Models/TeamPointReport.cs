﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class TeamPointReport
    {
        public string Id { get; set; }
        public string NameDetail { get; set; }
        public string TeamId { get; set; }
        public string TimeDetail { get; set; }
        public int TeamPointTotal { get; set; }
        public int PointLeft { get; set; }

        public virtual Team Team { get; set; }
    }
}
