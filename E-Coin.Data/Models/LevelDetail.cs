﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class LevelDetail
    {
        public string SubLevelId { get; set; }
        public string DetailId { get; set; }
        public int Value { get; set; }

        public virtual Detail Detail { get; set; }
        public virtual SubLevel SubLevel { get; set; }
    }
}
