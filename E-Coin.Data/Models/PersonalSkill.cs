﻿using System;
using System.Collections.Generic;

namespace E_Coin.Data.Models
{
    public partial class PersonalSkill
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberOfCertification { get; set; }
        public string EmpId { get; set; }

        public virtual Employee Emp { get; set; }
    }
}
