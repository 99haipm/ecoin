﻿using E_Coin.Data.Domains;
using E_Coin.Data.Models;
using E_Coin.Data.Repositories;
using E_Coin.Data.Uow;
using E_Coin.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Coin.WebApi.Controllers
{
    [ApiController]
    [Route("/api/team")]
    public class TeamController : BaseController
    {
        public TeamController(IUnitOfWork uow) : base(uow)
        {
        }

        [HttpGet]
        public IActionResult Get([FromQuery] TeamFilter filter, 
            [FromQuery]string sort, 
            [FromQuery]string[] fields,
            [FromQuery]int page =0,
            [FromQuery]int limit = -1)
        {
            try
            {
                var domain = _uow.GetService<TeamDomain>();
                if(fields.Length == 0)
                {
                    fields = new string[] { TeamFieldsDetail.INFO };
                }
                var result = domain.GetTeam(filter, sort, fields, page, limit);
                return Success(result);
            }catch(Exception ex)
            {
                return Error(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] TeamCreateModel model)
        {
            try
            {
                Team newTeam = _uow.GetService<TeamDomain>().Create(model);
                if(newTeam != null)
                {
                    _uow.SaveChanges();
                    return Success(newTeam.Id);
                }
                return BadRequest();

            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Update([FromQuery]string teamId, [FromBody] TeamUpdateModel model)
        {
            try
            {
                Team updated = _uow.GetService<TeamDomain>().Update(teamId ,model);
                if (updated != null)
                {
                    _uow.SaveChanges();
                    return Success(updated.Id);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete([FromQuery]string id)
        {
            try
            {
                Team updated = _uow.GetService<TeamDomain>().Delete(id);
                if (updated != null)
                {
                    _uow.SaveChanges();
                    return Success(updated.Id);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
    }
}
