﻿using E_Coin.Data.Domains;
using E_Coin.Data.Uow;
using E_Coin.Data.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Coin.WebApi.Controllers
{
    [ApiController]
    [Route("/api/user")]
    public class UserController : BaseController
    {
        public UserController(IUnitOfWork uow) : base(uow)
        {

        }

        [HttpPost]
        public IActionResult Register(UserCreateModel model)
        {
            try
            {
                var userDomain = _uow.GetService<UserDomain>();
                string id = userDomain.RegisterUser(model);
                return Success(id);
            }catch(Exception ex)
            {
                return Error(ex);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var token = Request.Headers["Authorization"];
                var userDomain = _uow.GetService<UserDomain>();
                var result = userDomain.GetUserDetail(token.ToString().Split(" ")[1]);
                return Ok(result);
            }
            catch(Exception ex)
            {
                return Error(ex);
            }
        }

        [HttpPut]
        public IActionResult Put()
        {
            return Ok();
        }
    }
}
