﻿using E_Coin.Data.Domains;
using E_Coin.Data.Uow;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Coin.WebApi.Controllers
{
    [ApiController]
    [Route("/jwt")]
    public class JwtController : BaseController
    {
        public JwtController(IUnitOfWork uow) : base(uow)
        {
         
        }

        [HttpGet]
        public IActionResult GetToken([FromQuery]string token)
        {
            try
            {

                var domain = _uow.GetService<JWTDomain>();
                var returnedToken = domain.DecodeJwt(token);
                return Success(returnedToken);

            }catch(Exception e)
            {
                return Error(e);
            }
        }


    }
}
