﻿using E_Coin.Data.Domains;
using E_Coin.Data.Extensions;
using E_Coin.Data.Models;
using E_Coin.Data.Uow;
using E_Coin.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;

namespace E_Coin.WebApi.Controllers
{
    [ApiController]
    [Route("/api/personalSkill")]
    public class PersonalSkillController : BaseController
    {
        public PersonalSkillController(IUnitOfWork uow) : base(uow)
        {

        }
        [HttpGet]
        public IActionResult Get([FromQuery]PersonalFilter filter,
            [FromQuery]string[] fields,
            [FromQuery]string sort,
            [FromQuery]int page = 0,
            [FromQuery]int limit = -1)
        {
            try
            {
                var domain = _uow.GetService<PersonalSkillDomain>();
                if (fields.Length == 0)
                {
                    fields = new string[] { TeamFieldsDetail.INFO };
                }
                var result = domain.GetPersonalSkill(filter, sort, fields, page, limit);
                return Success(result);
            }
            catch (Exception e)
            {
                return Error(e.Message);
            }

        }

        [HttpPost]
        public IActionResult Create([FromBody] PersonalSkillModel model)
        {
            try
            {
                PersonalSkill newPersonalSkill = _uow.GetService<PersonalSkillDomain>().Create(model);
                if (newPersonalSkill != null)
                {
                    _uow.SaveChanges();
                    return Success(newPersonalSkill.Id);
                }
                return BadRequest();
            }
            catch (Exception e)
            {
                return Error(e.Message);
            }
        }
        [HttpPut]
        public IActionResult Update([FromQuery]string perSkillID, [FromBody] PersonalSkillUpdate model)
        {
            try
            {
                PersonalSkill updatePerSkill = _uow.GetService<PersonalSkillDomain>().Update(perSkillID, model);
                if (updatePerSkill != null)
                {
                    _uow.SaveChanges();
                    return Success(perSkillID);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return Error(ex.Message);
            }
        }
        [HttpDelete]
        public IActionResult Delete([FromQuery]string perSkillID)
        {
            try
            {
                PersonalSkill deletePerSkill = _uow.GetService<PersonalSkillDomain>().Delete(perSkillID);
                if (deletePerSkill != null)
                {
                    _uow.SaveChanges();
                    return NoContent();
                }
                return BadRequest();

            }
            catch (Exception e)
            {

                return Error(e.Message);
            }
        }
    }
}
