﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Coin.WebAdmin.Pages
{
    public abstract class GeneralPageModel : PageModel
    {
        public abstract PageInfo PageInfo { get; }
    }
}
