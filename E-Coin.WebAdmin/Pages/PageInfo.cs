﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_Coin.WebAdmin.Pages
{
    public class PageInfo
    {
        public string Title { get; set; }
        public string ActiveMenu { get; set; }
        public string BackUrl { get; set; }

    }
}
