﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace E_Coin.WebAdmin.Pages.Home.Main
{
    public class IndexModel : GeneralPageModel
    {
        public override PageInfo PageInfo => new PageInfo
        {
            Title = "Tổng Quan",
            ActiveMenu = "main"

        };
        public void OnGet()
        {
            PageInfo.BackUrl = "/";
        }
    }
}