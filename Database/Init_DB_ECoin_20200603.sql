USE [master]
GO
/****** Object:  Database [ECoin]    Script Date: 6/3/2020 4:08:36 PM ******/
CREATE DATABASE [ECoin]
GO
ALTER DATABASE [ECoin] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ECoin].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ECoin] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ECoin] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ECoin] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ECoin] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ECoin] SET ARITHABORT OFF 
GO
ALTER DATABASE [ECoin] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ECoin] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ECoin] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ECoin] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ECoin] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ECoin] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ECoin] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ECoin] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ECoin] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ECoin] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ECoin] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ECoin] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ECoin] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ECoin] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ECoin] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ECoin] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ECoin] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ECoin] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ECoin] SET  MULTI_USER 
GO
ALTER DATABASE [ECoin] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ECoin] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ECoin] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ECoin] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ECoin] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ECoin] SET QUERY_STORE = OFF
GO
USE [ECoin]
GO
/****** Object:  Table [dbo].[ SubLevel]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ SubLevel](
	[Id] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PositionId] [varchar](50) NOT NULL,
	[SalaryLevel] [float] NOT NULL,
 CONSTRAINT [PK_Levels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[Id] [varchar](50) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Status] [char](10) NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Detail]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Detail](
	[Id] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Detail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Emp_Pay]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emp_Pay](
	[EmployeeId] [varchar](50) NOT NULL,
	[PayPeriodId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Emp_Pay] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC,
	[PayPeriodId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [varchar](50) NOT NULL,
	[Fullname] [varchar](50) NOT NULL,
	[Address] [varchar](50) NOT NULL,
	[DateJoin] [datetime] NOT NULL,
	[Sex] [bit] NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Status] [char](10) NOT NULL,
	[BOD] [date] NOT NULL,
	[CompanyId] [varchar](50) NOT NULL,
	[LevelId] [varchar](50) NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Level]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Level](
	[Id] [varchar](50) NOT NULL,
	[Description] [varchar](max) NULL,
	[HourRate] [int] NOT NULL,
	[MinHotPoint] [int] NOT NULL,
	[BasicCash] [int] NOT NULL,
	[CoinUpTo] [int] NOT NULL,
	[UpLevelReward] [int] NOT NULL,
	[RetainerBonus] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Level_Detail]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Level_Detail](
	[SubLevelId] [varchar](50) NOT NULL,
	[DetailId] [varchar](50) NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_Level_Detail] PRIMARY KEY CLUSTERED 
(
	[SubLevelId] ASC,
	[DetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LevelHistory]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LevelHistory](
	[UserId] [varchar](50) NOT NULL,
	[SublevelId] [varchar](50) NOT NULL,
	[Datetime] [datetime] NOT NULL,
	[FromLevel] [varchar](50) NULL,
	[ToLevel] [varchar](50) NULL,
 CONSTRAINT [PK_LevelHistories] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[SublevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayCheck]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayCheck](
	[Id] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[EmpId] [varchar](50) NOT NULL,
	[DateFinalize] [datetime] NOT NULL,
	[P1] [int] NOT NULL,
	[P2] [int] NOT NULL,
	[P3] [int] NOT NULL,
	[Total] [float] NOT NULL,
	[Performance] [char](10) NOT NULL,
	[EvaluateRetrainer] [char](10) NOT NULL,
	[TeamPointShare] [int] NOT NULL,
	[UpLevelReward] [int] NULL,
	[WorkingHour] [float] NOT NULL,
	[PayPeriodId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PayChecks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayPeriod]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayPeriod](
	[Id] [varchar](50) NOT NULL,
	[BeginTime] [date] NOT NULL,
	[EndTime] [date] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Total] [float] NOT NULL,
	[CompanyId] [varchar](50) NULL,
 CONSTRAINT [PK_PayPeriods] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonalSkill]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonalSkill](
	[Id] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[NumberOfCertification] [int] NOT NULL,
	[EmpId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PersonalSkills] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[PermissionDetail] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Team]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Team](
	[Id] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[TeamPoint] [int] NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeamDetail]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeamDetail](
	[EmpId] [varchar](50) NOT NULL,
	[TeamId] [varchar](50) NOT NULL,
	[RoleInTeam] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TeamDetails] PRIMARY KEY CLUSTERED 
(
	[EmpId] ASC,
	[TeamId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TeamPointReport]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeamPointReport](
	[Id] [varchar](50) NOT NULL,
	[NameDetail] [varchar](50) NOT NULL,
	[TeamId] [varchar](50) NOT NULL,
	[TimeDetail] [varchar](50) NOT NULL,
	[TeamPointTotal] [int] NOT NULL,
	[PointLeft] [int] NOT NULL,
 CONSTRAINT [PK_TeamPointReport] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [varchar](50) NOT NULL,
	[WalletId] [varchar](50) NOT NULL,
	[TimeChange] [datetime] NOT NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [varchar](50) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Fullname] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Facebook] [varchar](50) NULL,
	[Phone] [varchar](50) NOT NULL,
	[Address] [varchar](50) NOT NULL,
	[Status] [char](10) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserId] [varchar](50) NOT NULL,
	[RoleId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Wallet]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wallet](
	[Id] [varchar](50) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[Value] [int] NOT NULL,
	[EmpId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Wallets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WalletType]    Script Date: 6/3/2020 4:08:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WalletType](
	[Id] [varbinary](50) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[WalletId] [varchar](50) NULL,
 CONSTRAINT [PK_Wallet_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Company] ([Id], [Name], [Status]) VALUES (N'1', N'FPT SoftWare HCM', N'Active    ')
INSERT [dbo].[Employee] ([Id], [Fullname], [Address], [DateJoin], [Sex], [Phone], [Status], [BOD], [CompanyId], [LevelId]) VALUES (N'386215bb-ef97-4dc6-9068-8a228414a216', N'Pham Minh Hai', N'address', CAST(N'2020-06-03T00:00:00.000' AS DateTime), 1, N'0907386696', N'Active    ', CAST(N'1999-07-18' AS Date), N'1', NULL)
INSERT [dbo].[Level] ([Id], [Description], [HourRate], [MinHotPoint], [BasicCash], [CoinUpTo], [UpLevelReward], [RetainerBonus], [Name]) VALUES (N'1', NULL, 6, 250, 25, 250, 200, 200, N'Preseed
')
INSERT [dbo].[PersonalSkill] ([Id], [Name], [Description], [NumberOfCertification], [EmpId]) VALUES (N'5992f9a6-e471-490c-8ba5-4b6747d931d3', N'china', N'desc', 0, N'386215bb-ef97-4dc6-9068-8a228414a216')
INSERT [dbo].[PersonalSkill] ([Id], [Name], [Description], [NumberOfCertification], [EmpId]) VALUES (N'9cd57d39-1fc2-41f4-82d8-4214070f8de0', N'English Ielts', N'desc', 0, N'386215bb-ef97-4dc6-9068-8a228414a216')
INSERT [dbo].[PersonalSkill] ([Id], [Name], [Description], [NumberOfCertification], [EmpId]) VALUES (N'a10cb026-983a-4377-8cc3-dea71b55ad54', N'Japanese', N'asdsad', 20, N'386215bb-ef97-4dc6-9068-8a228414a216')
INSERT [dbo].[Role] ([Id], [Name], [PermissionDetail]) VALUES (N'1', N'Admin', N'Quyền hành cao nhất')
INSERT [dbo].[Role] ([Id], [Name], [PermissionDetail]) VALUES (N'2', N'User', N'Nhân viên của cty')
INSERT [dbo].[Role] ([Id], [Name], [PermissionDetail]) VALUES (N'3', N'Manager', N'Quản lý team')
INSERT [dbo].[Team] ([Id], [Name], [Description], [TeamPoint], [Status]) VALUES (N'0f8e4581-c73c-4254-a47f-796e65bcc081', N'edited', N'string', 0, N'disable')
INSERT [dbo].[Team] ([Id], [Name], [Description], [TeamPoint], [Status]) VALUES (N'183b8511-569e-4241-9d3f-5d3ab6367a78', N'team 1', N'desc', 0, N'active')
INSERT [dbo].[Team] ([Id], [Name], [Description], [TeamPoint], [Status]) VALUES (N'31d72e53-66c8-486a-9a1a-9b3607a665ed', N'Super team 3', N'ahihi day la desc', 0, N'disable')
INSERT [dbo].[Team] ([Id], [Name], [Description], [TeamPoint], [Status]) VALUES (N'557363fa-4c34-46de-95c3-483d3e212a54', N'Team 2', N'this is desc', 0, N'active')
INSERT [dbo].[Team] ([Id], [Name], [Description], [TeamPoint], [Status]) VALUES (N'574e8d04-de1e-4e0f-81a1-37c605391f72', N'UniIncubator', N'day la desc', 0, N'active')
INSERT [dbo].[Team] ([Id], [Name], [Description], [TeamPoint], [Status]) VALUES (N'9d48c386-1d85-487f-834e-63c58e7766c7', N'Team 6', N'this is desc', 0, N'active')
INSERT [dbo].[Team] ([Id], [Name], [Description], [TeamPoint], [Status]) VALUES (N'd0fcf7b1-6e3e-417b-b3a8-23669f6bd7a9', N'Team 4', N'this is desc', 0, N'active')
INSERT [dbo].[Team] ([Id], [Name], [Description], [TeamPoint], [Status]) VALUES (N'fb6e3b82-00f1-4ac3-b670-618d5f4f2e62', N'Team 5', N'this is desc', 0, N'active')
INSERT [dbo].[User] ([Id], [Username], [Fullname], [Email], [Facebook], [Phone], [Address], [Status]) VALUES (N'386215bb-ef97-4dc6-9068-8a228414a216', N'haipmse130332@fpt.edu.vn', N'Phạm Minh Hải', N'99email.com', N'null', N'0907386696', N'Binh Tan', N'working   ')
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (N'386215bb-ef97-4dc6-9068-8a228414a216', N'1')
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (N'386215bb-ef97-4dc6-9068-8a228414a216', N'2')
ALTER TABLE [dbo].[ SubLevel]  WITH CHECK ADD  CONSTRAINT [FK_Levels_Positions] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Level] ([Id])
GO
ALTER TABLE [dbo].[ SubLevel] CHECK CONSTRAINT [FK_Levels_Positions]
GO
ALTER TABLE [dbo].[Emp_Pay]  WITH CHECK ADD  CONSTRAINT [FK_Emp_Pay_Employees] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[Emp_Pay] CHECK CONSTRAINT [FK_Emp_Pay_Employees]
GO
ALTER TABLE [dbo].[Emp_Pay]  WITH CHECK ADD  CONSTRAINT [FK_Emp_Pay_PayPeriods] FOREIGN KEY([PayPeriodId])
REFERENCES [dbo].[PayPeriod] ([Id])
GO
ALTER TABLE [dbo].[Emp_Pay] CHECK CONSTRAINT [FK_Emp_Pay_PayPeriods]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Level] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Level] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Level]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employees_Companies]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Users] FOREIGN KEY([Id])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employees_Users]
GO
ALTER TABLE [dbo].[Level_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Level_Detail_ SubLevel] FOREIGN KEY([SubLevelId])
REFERENCES [dbo].[ SubLevel] ([Id])
GO
ALTER TABLE [dbo].[Level_Detail] CHECK CONSTRAINT [FK_Level_Detail_ SubLevel]
GO
ALTER TABLE [dbo].[Level_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Level_Detail_Detail] FOREIGN KEY([DetailId])
REFERENCES [dbo].[Detail] ([Id])
GO
ALTER TABLE [dbo].[Level_Detail] CHECK CONSTRAINT [FK_Level_Detail_Detail]
GO
ALTER TABLE [dbo].[LevelHistory]  WITH CHECK ADD  CONSTRAINT [FK_LevelHistories_Employees] FOREIGN KEY([UserId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[LevelHistory] CHECK CONSTRAINT [FK_LevelHistories_Employees]
GO
ALTER TABLE [dbo].[LevelHistory]  WITH CHECK ADD  CONSTRAINT [FK_LevelHistories_Levels] FOREIGN KEY([SublevelId])
REFERENCES [dbo].[ SubLevel] ([Id])
GO
ALTER TABLE [dbo].[LevelHistory] CHECK CONSTRAINT [FK_LevelHistories_Levels]
GO
ALTER TABLE [dbo].[PayCheck]  WITH CHECK ADD  CONSTRAINT [FK_PayChecks_Employees] FOREIGN KEY([EmpId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[PayCheck] CHECK CONSTRAINT [FK_PayChecks_Employees]
GO
ALTER TABLE [dbo].[PayCheck]  WITH CHECK ADD  CONSTRAINT [FK_PayChecks_PayPeriods] FOREIGN KEY([PayPeriodId])
REFERENCES [dbo].[PayPeriod] ([Id])
GO
ALTER TABLE [dbo].[PayCheck] CHECK CONSTRAINT [FK_PayChecks_PayPeriods]
GO
ALTER TABLE [dbo].[PayPeriod]  WITH CHECK ADD  CONSTRAINT [FK_PayPeriod_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([Id])
GO
ALTER TABLE [dbo].[PayPeriod] CHECK CONSTRAINT [FK_PayPeriod_Company]
GO
ALTER TABLE [dbo].[PersonalSkill]  WITH CHECK ADD  CONSTRAINT [FK_PersonalSkill_Employee] FOREIGN KEY([EmpId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[PersonalSkill] CHECK CONSTRAINT [FK_PersonalSkill_Employee]
GO
ALTER TABLE [dbo].[TeamDetail]  WITH CHECK ADD  CONSTRAINT [FK_TeamDetails_Employees] FOREIGN KEY([EmpId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[TeamDetail] CHECK CONSTRAINT [FK_TeamDetails_Employees]
GO
ALTER TABLE [dbo].[TeamDetail]  WITH CHECK ADD  CONSTRAINT [FK_TeamDetails_Teams] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Team] ([Id])
GO
ALTER TABLE [dbo].[TeamDetail] CHECK CONSTRAINT [FK_TeamDetails_Teams]
GO
ALTER TABLE [dbo].[TeamPointReport]  WITH CHECK ADD  CONSTRAINT [FK_TeamPointReport_Teams] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Team] ([Id])
GO
ALTER TABLE [dbo].[TeamPointReport] CHECK CONSTRAINT [FK_TeamPointReport_Teams]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transactions_Wallets] FOREIGN KEY([WalletId])
REFERENCES [dbo].[Wallet] ([Id])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transactions_Wallets]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRoles_Roles]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRoles_Users]
GO
ALTER TABLE [dbo].[Wallet]  WITH CHECK ADD  CONSTRAINT [FK_Wallets_Employees] FOREIGN KEY([EmpId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[Wallet] CHECK CONSTRAINT [FK_Wallets_Employees]
GO
ALTER TABLE [dbo].[WalletType]  WITH CHECK ADD  CONSTRAINT [FK_WalletType_Wallet] FOREIGN KEY([WalletId])
REFERENCES [dbo].[Wallet] ([Id])
GO
ALTER TABLE [dbo].[WalletType] CHECK CONSTRAINT [FK_WalletType_Wallet]
GO
USE [master]
GO
ALTER DATABASE [ECoin] SET  READ_WRITE 
GO
